import 'regenerator-runtime/runtime';
import nod from '../common/nod';
import PageManager from '../page-manager';
import $ from 'jquery';

export default class CustomDemo extends PageManager {

    constructor(context) {
        super(context);           
        this.customProductId = this.context.customProduct.id;
        this.giftOptionId = "attribute_text_" + this.context.giftOptionId;
        this.$giftOptionField = document.getElementById(this.giftOptionId);
        this.$submitButton = document.getElementById("form-action-addToCart");
        this.$customEmailField = document.getElementById('attribute_text_custom_email');
        this.$showGiftEmailOption = document.getElementById("show_gift_email_option");
        this.$showGiftOptionField = document.getElementById('show_gift_option')
        this.nod = nod();
    }

/**
 * 
 * Clear all custom fields when the page loaded
 * 
 */

    clearCustomData() {
        this.$showGiftOptionField.checked = false;
        this.$giftOptionField.value = "";
        document.getElementById('not_show_gift_option').checked = true;
        this.$showGiftEmailOption.checked = false;
        this.$customEmailField.value = "";
        document.getElementById('not_show_gift_email').checked = true;
        this.hideGiftTextField();
        this.hideGiftEmailField();
    }

/**
 * 
 * Show gift text option and initializes field validation 
 * 
 */

    showGiftTextField() {
        $('#giftFieldForm').show();
        this.nod.configure({
            submit: '#form-action-addToCart',
            disableSubmit: true,             
        });
        const patternstr = '^[A-Za-z0-9., \n]{0,300}$';
        let constraint = new RegExp(patternstr, "");

        this.nod.add([{
            selector: this.$giftOptionField,
            validate: function (callback, value) {
                callback(constraint.test(value))
            },
            errorMessage: 'Unknown or restricted symbol: A congratulatory inscription should consist of letters, commas, and dots.',
        },{
            selector: this.$giftOptionField,
            validate: 'max-length:200',
            errorMessage: 'Too many symbols. Please, make your text shorter. 200 symbols maximum.',    
        },{
            selector: this.$giftOptionField,
            validate: 'min-length:1',
            errorMessage: 'Please, write your gift message.',    
        }]);       
    }

/**
 * 
 * Hide gift option and cancel field validation 
 * 
 */

    hideGiftTextField() {
        document.getElementById('giftFieldForm').style.display = "none";
        this.$giftOptionField.value = "";
        this.nod.configure({
            submit: '#form-action-addToCart',
            disableSubmit: false,             
        });
        this.nod.remove(this.$giftOptionField);
    }

/**
 * 
 * Show gift email option and initializes field validation 
 * 
 */    

    showGiftEmailTextField() {
        document.getElementById('GiftEmailField').style.display = "block";
        this.nod.add([{
            selector: this.$customEmailField,
            validate: 'email',                    
            errorMessage: 'Please enter a valid email address.',
        },{
            selector: this.$customEmailField,
            validate: 'min-length:1',
            errorMessage: 'Please, write your email address.',    
        }]);
    }

/**
 * 
 * Hide gift email option and cancel field validation 
 * 
 */    

    hideGiftEmailField() {
        document.getElementById('GiftEmailField').style.display = "none";
        this.nod.remove(this.$customEmailField);   
        this.$customEmailField.value = "";
    }

/**
 * 
 * Returns the result of gift text input field validation  
 * 
 */    

    checkGiftText() {
        this.nod.performCheck();
        return this.nod.getStatus([this.$giftOptionField]);
    }

/**
 * 
 * Returns the result of gift email input field validation  
 * 
 */    


    checkCustomMailText() {
        this.nod.performCheck();
        return this.nod.getStatus([this.$customEmailField]);            
    }

/**
 * 
 * Calls the function to clear fields when closing the trash modal window.
 * 
 * @callback this.clearCustomData 
 * 
 */    


    checkCloseCart(e) {
        if (e.target.classList.contains('modal-close') || e.target.parentElement.classList.contains('modal-close')) {
            this.clearCustomData();
        };
        if (e.target.dataset.revealClose === "") {
            this.clearCustomData();
        }
    }

/**
 * 
 * Calls the function to clear fields when closing the trash modal window.
 * 
 * @callback this.clearCustomData 
 * 
 */    


    checkCloseCart2(e) {
        if (e.target.classList.contains('modal-background') && document.getElementById('previewModal').style.display == "block") {
            this.clearCustomData();
        }
    }

/**
 * 
 * Adds a way to send congratulations to the gift input field, if all fields are filled in correctly;
 * 
 * 
 */    

    checkGiftTextSubmitted() {

        if (this.$showGiftOptionField.checked && this.$showGiftEmailOption.checked && this.checkCustomMailText() == "valid" && this.checkGiftText() == "valid") {
            this.$giftOptionField.value +=  " Send congratulatory on Email:" + this.$customEmailField.value;
        } else if (this.$showGiftOptionField.checked && !this.$showGiftEmailOption.checked && this.checkGiftText() == "valid") {
            this.$giftOptionField.value += " Print congratulatory on a gift card";            
        }         
        document.body.addEventListener('click', this.checkCloseCart2.bind(this), false);
    }

    onReady() {
        if (document.getElementById("custom_options_added")) {           
            this.clearCustomData();           
            document.getElementById("not_show_gift_option").onchange = this.hideGiftTextField.bind(this);
            this.$showGiftOptionField.onchange = this.showGiftTextField.bind(this);
            document.getElementById("not_show_gift_email").onchange = this.hideGiftEmailField.bind(this);
            this.$showGiftEmailOption.onchange = this.showGiftEmailTextField.bind(this);
            document.getElementById("form-action-addToCart").onclick = this.checkGiftTextSubmitted.bind(this);
            document.getElementById("previewModal").onclick = this.checkCloseCart.bind(this);
        }
    }
}
