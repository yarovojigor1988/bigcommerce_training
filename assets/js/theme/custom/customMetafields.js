import 'regenerator-runtime/runtime';
import nod from '../common/nod';
import PageManager from '../page-manager';
import $ from 'jquery';
import swal from '../global/sweet-alert';


export default class CustomDemo extends PageManager {

    constructor(context) {
        super(context);
        this.priseChangeOptionId;
        this.priseChangeValueArr;
        this.priseChangeValueId;
        this.productPrice = this.context.customProduct.price.without_tax.value;
        this.$submitButton = $("#form-action-addToCart");
        this.customProductId = this.context.customProduct.id;
        this.nod = nod();
        this.engravingOptionId = `#attribute_text_${this.context.engravingOptionId}`;
        this.$engravingOptionfield = $(this.engravingOptionId)[0];
        this.$showEngravingOption = $("#show_engraving_option");
        this.$form = $('form[data-cart-item-add]');
        this.optionsString = "";
    }

/**
 * Creates a shopping cart
 * 
 * @param {String} lineitems 
 * 
 * @returns {Promise}
 */


    async createNewCart(lineItems) {
        const response = await fetch(`/api/storefront/carts`, {
            credentials: "include",
            method: "POST",
            body: lineItems
        });
        const data = await response.json();
        if (!response.ok) {
            return Promise.reject("There was an issue adding items to your cart. Please try again.")
        } 
    }

/**
 * Updates a shopping cart
 * 
 * @param {String} lineitems 
 * 
 * @returns {Promise}
 */

    async addToExistingCart(cart_id, lineItems) {
        const response = await fetch(`/api/storefront/carts/${cart_id}/items`, {
            credentials: "include",
            method: "POST",
            body: lineItems
        });
        const data = await response.json();
        if (!response.ok) {
            return Promise.reject("There was an issue adding items to your cart. Please try again.")
        } 
    }

/**
 * 
 * Clearing fields on page load.
 * 
 */        

    clearFielsd() {
        $("#not_show_engraving_option").checked = true;
        this.$engravingOptionfield.value = "";
    }

/**
 * 
 * Changing product price when engraving field changes. Set this.priseChangeValueId depending on the length engraving field value.
 * 
 */      

    changeEngravingPrice() {
        let length1 = this.$engravingOptionfield.value.replace(/\s+/g, '').length;
        if (length1 > 0 && length1 <= 50){
            this.priseChangeValueId = this.priseChangeValueArr[length1-1].id;
        } else {
            this.priseChangeValueId = `""`;
        }
        let newPriceNode = $('.productView-price')[0];
        newPriceNode.innerHTML = "₴";
        let newPrice = (this.productPrice + length1*2).toFixed(2);
        newPriceNode.innerHTML += newPrice;
    }

/**
 * 
 * Initializes Engraving field validation 
 * 
 */        

    validationInit() {

        const patternstr = '^[A-Za-z., \n]{0,300}$';
        let constraint = new RegExp(patternstr, "");
        this.nod.add([{
            selector: this.$engravingOptionfield,
            validate: function (callback, value) {
                callback(constraint.test(value))
            },
            errorMessage: this.context.productCustomSymbolError,
        },{
            selector: this.$engravingOptionfield,
            validate: 'max-length:50',
            errorMessage: this.context.custom_max_length_error,    
        },{
            selector: this.$engravingOptionfield,
            validate: 'min-length:1',
            errorMessage: this.context.custom_min_length_error,    
        }]);     
    }  

/**
 * 
 * Show Engraving option and configurate nod.validator to disable button "Add to cart" if validation filed.
 * 
 */    

    showEngravingTextField() {
        $('#engravingFieldForm').show();                
        this.nod.configure({
            submit: '#form-action-addToCart',
            disableSubmit: true,             
        });

    }

    checkValidation() {
        this.nod.performCheck();
        return this.nod.getStatus([this.$engravingOptionfield]); 
    }

/**
 * Checking the cart. If the cart exists, we update it, if not - create new cart.
 * 
 * @param {String} lineitems 
 * 
 * @callback this.addToExistingCart
 * @callback this.createNewCart
 */

    createCustomCart(lineItems) {
        fetch(`/api/storefront/cart`)
        .then(response => response.json())
        .then(cart => {
            if (cart.length > 0) {
                return this.addToExistingCart(cart[0].id, lineItems)
            } else {
                return this.createNewCart(lineItems)
            }
        })
        .then(() => window.location = '/cart.php')
        .catch(err => console.log(err));
    }

/**
 * If the fields are filled in correctly, or if the engraving option is not selected, then the function for creating a cart is called
 * 
 * @callback this.createCustomCart
 */


    customSubmit(e) {
        e.preventDefault();
        let lineItems;
        let itemQuantity = document.getElementById('qty[]').value;

        if (!this.$form[0].checkValidity()) {
            return swal.fire({
                text: this.context.custom_validation_form_error,
                icon: 'error',
            });      
        }

        this.getFormOptions();
              
        if (this.checkValidation() == "valid" && this.$showEngravingOption[0].checked) {
            lineItems = `{"lineItems":[{"productId": ${this.customProductId}, "quantity": ${itemQuantity}, "option_selections": [ ${this.optionsString}, { "option_id": ${this.priseChangeOptionId}, "optionValue": ${this.priseChangeValueId}}]}]}`;
        } else if ($("#not_show_engraving_option").checked && this.optionsString.length == 0){
            lineItems = `{"lineItems":[{"productId": ${this.customProductId}, "quantity": ${itemQuantity}}]}`;
        } else if ($("#not_show_engraving_option").checked && this.optionsString.length > 0){
            lineItems = `{"lineItems":[{"productId": ${this.customProductId}, "quantity": ${itemQuantity}, "option_selections": [ ${this.optionsString} ]}]}`;
        } else {
            return
        }        

        this.createCustomCart(lineItems);
    }

/**
 * 
 * Hide engraving text field. Configurate nod.validator to not disable button "Add to cart"
 * 
 */

    hideEngravingTextField() {
        $('#engravingFieldForm').hide();
        this.nod.configure({
            submit: '#form-action-addToCart',
            disableSubmit: false,             
        });    
        this.$engravingOptionfield.value = "";
        this.changeEngravingPrice();
    }

/**
 * 
 * Set variables from context to constructor.
 * 
 */

    optionsSet() {       
        if (this.context.customProduct.options.filter(option => option.display_name === "Engraving price")[0]) {
            this.priseChangeOptionId = this.context.customProduct.options.filter(option => option.display_name === "Engraving price")[0].id;
            this.priseChangeValueArr = this.context.customProduct.options.filter(option => option.display_name === "Engraving price")[0].values;
            this.priseChangeValueId = `""`;
        }
    }

/**
 * 
 * gets all product options
 * 
 * @returns string
 */

    getFormOptions(){
        let strStart = this.$form.serialize();
        let str1 = strStart.split("&");
        let str2 = str1.filter((el)=>{
            return el.includes("attribute")
        })

        let str3 = str2.map(function(element) {
            element = element.replace('attribute%5B', '');
            element = element.split('%5D=');
            return element;

        })
        let str4 = str3.filter((el)=>{
            let a = true;
            el.forEach(element => {
                if (element.length == 0) {a = false}
            });
            return a;
        })
        let optionsString = "";
        str4.forEach(element => {
            if (optionsString.length == 0) {
                optionsString = `{"option_id": ${element[0]}, "optionValue": "${element[1]}"}`
            } else {
                optionsString = optionsString + ", " + `{"option_id": ${element[0]}, "optionValue": "${element[1]}"}`;
            }
        })
        this.optionsString = optionsString;
    }

    onReady() {
        if ($("#engraving_options_added").length>0) {
            this.clearFielsd();
            this.optionsSet();
            this.validationInit();
            this.$submitButton[0].onclick = this.customSubmit.bind(this);
            $("#not_show_engraving_option")[0].onchange = this.hideEngravingTextField.bind(this);
            this.$showEngravingOption[0].onchange = this.showEngravingTextField.bind(this);
            this.$engravingOptionfield.oninput = this.changeEngravingPrice.bind(this);
        }
    }
}