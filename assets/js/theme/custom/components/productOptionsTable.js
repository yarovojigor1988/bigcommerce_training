import React from 'react';
import { render } from 'react-dom';
import ProductOptionsRow from './productOptionsRow';
import ProductTotal from './productTotal';

export default class ProductOptionsTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            totalCost: 0,
            variants: [],
        }
    }

    changeTotalcost = (variantCostObjectId, variantCostObjectValue) => {
        let objIndex = this.state.variants.findIndex((obj => obj.id == variantCostObjectId));
        if (objIndex > -1) {
            this.state.variants[objIndex].value = variantCostObjectValue;
        } else {
            this.state.variants.push({id: variantCostObjectId, value: variantCostObjectValue})
        }
        let totalCostValue = this.state.variants
            .map((variant) => variant.value)
            .reduce((prev, next) => prev + next);
        
        this.setState({
            totalCost: totalCostValue
        })        
    }

    render() {
        return (
            <div>
                {this.props.variants.map((variant) => { return (
                <ProductOptionsRow 
                    key = {variant.id} 
                    variant = {variant} 
                    changeTotalcost = {this.changeTotalcost}/>
                )})}
                <ProductTotal totalCost={this.state.totalCost}/>
            </div>
        );  
    }
  
}