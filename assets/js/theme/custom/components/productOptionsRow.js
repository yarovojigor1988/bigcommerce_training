import React from 'react';
import { render } from 'react-dom';
import ProductOptionsInput from './productOptionsInput';
import ProductVariantOption from './productVariantOption';
import ProductAvailabilityRow from './productAvailabilityRow';

export default class ProductOptionsRow extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            variantCost: 0,
        }
        this.updateQuantity = this.updateQuantity.bind(this);
    }

    updateQuantity(value){
        let variantCost = value * this.props.variant.prices.price.value;
        this.setState({
            variantCost: variantCost
        })        
        this.props.changeTotalcost(this.props.variant.entityId, variantCost);
    }

    render() {
        let imgSrc = this.props.variant.defaultImage ? this.props.variant.defaultImage.url : "";
        let availableToSell = this.props.variant.inventory?.aggregated.availableToSell != null ? this.props.variant.inventory.aggregated.availableToSell : Infinity;
        return (
        
            <div className='variants_form_row'>               
                <div className='variants_form_cell'><img src={imgSrc}/></div>
                <div className='variants_form_cell'>{this.props.variant.sku}</div>
                <div className='variants_form_cell'>{this.props.variant.prices.price.value} {this.props.variant.prices.price.currencyCode}</div>
                <div className='variants_form_cell'>
                    {this.props.variant.options.map((variantOption) => { return (
                    <ProductVariantOption key={variantOption.entityId} variantOption={variantOption}/>
                    )})}
                </div>
                <ProductAvailabilityRow inventory = {this.props.variant.inventory}/>
                <ProductOptionsInput 
                availableToSell = {availableToSell} 
                updateQuantity = {this.updateQuantity}
                />
            </div>
        );
    }

}