import React from 'react';

export default function ProductTotal(props) {
     
    return (
        <div className="total-holder">
            <button id="variantsFormSubmit" className="button button--primary">Submit</button>
            <div className="total-price">
                <span>Total price: </span>
                <span id="totalPriceValue">{props.totalCost}</span>
                <span> UAH</span>            
            </div>
        </div>
    );
}