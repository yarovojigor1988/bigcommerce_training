import React from 'react';

export default function ProductAvailabilityRow(props) {

    let textAvailability = props.inventory?.aggregated.availableToSell != null ? props.inventory.aggregated.availableToSell : "N/A"
       
    return (
        <div className='variants_form_cell'>In stock: {textAvailability}</div>
    );
}