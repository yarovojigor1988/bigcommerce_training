import React from 'react';

export default function ProductVariantOption(props) {

    let displayName = props.variantOption.displayName;
    let value = props.variantOption.values[0].label;
       
    return (        
        <p>               
            {displayName}: {value}
        </p>
    );
}