import React from 'react';
import { render } from 'react-dom';

export default class ProductOptionsInput extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: ''
        }
    }
  
    handleChange(evt) {
        const inputValue = (evt.target.validity.valid) ? evt.target.value : this.state.inputValue;          
        this.setState({ inputValue });
        this.props.updateQuantity(inputValue);
    }

    render() {
       
        return (
            <div className='variants_form_cell'><input 
                    type='text' 
                    className='qtyField'
                    min='0'
                    max={this.props.availableToSell}
                    value={this.state.inputValue} 
                    onChange={this.handleChange.bind(this)}
                    pattern='[0-9]*'/>
            </div>
        );
    }
}