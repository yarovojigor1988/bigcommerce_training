import PageManager from '../page-manager'
import 'regenerator-runtime/runtime';

export default class CustomDemo extends PageManager {
    onReady () {

        let lineItems = [];

        async function createNewCart(lineItems) {
            const response = await fetch(`/api/storefront/carts`, {
                credentials: "include",
                method: "POST",
                body: JSON.stringify({lineItems: lineItems})
            });
            const data = await response.json();
            if (!response.ok) {
                return Promise.reject("There was an issue adding items to your cart. Please try again.")
            } 
        }
        async function addToExistingCart(cart_id, lineItems) {
            const response = await fetch(`/api/storefront/carts/${cart_id}/items`, {
                credentials: "include",
                method: "POST",
                body: JSON.stringify({lineItems: lineItems})
            });
            const data = await response.json();
            if (!response.ok) {
                return Promise.reject("There was an issue adding items to your cart. Please try again.")
            } 
        }

        function addToCart(){
            fetch(`/api/storefront/cart`)
            .then(response => response.json())
            .then(cart => {
                if (cart.length > 0) {
                    return addToExistingCart(cart[0].id, lineItems)
                } else {
                    return createNewCart(lineItems)
                }
            })
            .then(() => window.location = '/cart.php')
            .catch(err => console.log(err));
        }
        
        function updateItem(newItem) {
            let objIndex = -1;
            objIndex = lineItems.findIndex((obj => obj.product_id == newItem.product_id));
            if (objIndex > -1) {
                lineItems[objIndex].quantity = newItem.quantity;
            } else {
                lineItems.push(newItem)
            }
        }

        function deleteItem(item) {
            let objIndex = -1;
            objIndex = lineItems.findIndex((obj => obj.product_id == item));
            if (objIndex > -1) {
                lineItems.splice(objIndex, 1);
            } else {
                return
            }
        }

        function showErrorMessage(element, message) {
            let newElement = document.createElement("div");
            newElement.innerHTML = message;              
            newElement.classList.add("custom-form-input-error");
            element.parentNode.insertBefore(newElement, element.nextSibling);
            setTimeout(function(){newElement.remove()}, 2000)
        }

        function checkAddButton() {
            let enableAddButton = true;
            document.querySelectorAll(".custom-form-quantity").forEach(element => {
                if (element.dataset.validated === "false"){
                    enableAddButton = false;
                }
            });
            if (enableAddButton && lineItems.length > 0) {
                document.querySelectorAll('[data-event="add-products-to-cart"]').forEach(element => {
                    element.disabled = false;
                });
            } else {
                document.querySelectorAll('[data-event="add-products-to-cart"]').forEach(element => {
                    element.disabled = true;
                });
            }
        }

        function changeTotalcost() {
            let totalCost = 0;
            let fielsdHasErrors = false;
            document.querySelectorAll(".custom-form-quantity").forEach(element => {
                if (element.dataset.validated === "false"){
                    totalCost = "сorrect fields errors";
                    fielsdHasErrors = true;

                } else if (!fielsdHasErrors) {
                    let price1 = element.dataset.productPrice * element.value;
                    totalCost += price1;
                }
            });

            if (fielsdHasErrors) {
                document.querySelectorAll(".custom-form-currency").forEach(element => {
                    element.style.display = "none";
                });
            } else {
                document.querySelectorAll(".custom-form-currency").forEach(element => {
                    element.style.display = "inline";
                });
            }

            document.querySelectorAll('[data-id="custom-total-cost"]').forEach(element => {
                element.innerHTML = totalCost;
            });

        }

        function validateInput() {

            let item;
            let reg = new RegExp('^[0-9]+$');
            if (reg.test(this.value) && this.value > 0 && this.value <= 10) {
                item = {"product_id": parseInt(this.dataset.productId), "quantity": parseInt(this.value)}
                this.style.border = null;
                this.dataset.validated = "true";
            } else if (this.value == 0 || this.value == "") {
                deleteItem(this.dataset.productId);
                this.style.border = null;
                this.dataset.validated = "true";
            } else if (reg.test(this.value) && this.value > 10) {
                this.style.border = "2px solid red";
                let message = "The maximum value is 10";
                showErrorMessage(this, message);
                item = "error";
                deleteItem(this.dataset.productId);
                this.dataset.validated = "false";
            } else {
                this.style.border = "2px solid red";
                let message = "Please input correct number";
                showErrorMessage(this, message);
                item = "error";
                deleteItem(this.dataset.productId);
                this.dataset.validated = "false";
            }

            if (typeof(item) === "object" && lineItems.length === 0){
            lineItems.push(item);
            } else if (typeof(item) === "object" && lineItems.length > 0){
                updateItem(item);
            }
            changeTotalcost();
            checkAddButton();
        }

        document.querySelectorAll(".custom-form-quantity").forEach(input => {
            input.addEventListener("change", validateInput, false);
            input.addEventListener("input", function(){
                setTimeout(validateInput.bind(this), 2000);               
            });
        });

        document.querySelectorAll('[data-event="add-products-to-cart"]').forEach(element => {
            element.addEventListener("click", addToCart)
        })
    }
}