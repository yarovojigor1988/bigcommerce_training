import PageManager from '../page-manager'
import 'regenerator-runtime/runtime';
import React from 'react';
import initApolloClient from '../global/graphql/client';
import flattenGraphQLResponse from 'humanize-graphql-response';
import { render } from 'react-dom';
import ProductOptionsTable from './components/productOptionsTable';
import swal from '../global/sweet-alert';
import customProductOptions from '../../gql/customProductOptions.gql'

export default class CustomDemo extends PageManager {

    constructor(context) {
        super(context);
        this.productVariants = [];
        this.productId = this.context.productId;
        this.productInStock = this.context.productInStock;
        this.gqlClient = initApolloClient(this.context.storefrontAPIToken);
        this.pageSize = 50;

    }

/** Gets product options. When all options are received, starts rendering of the elements. 
 * 
 * @see ./gql/customProductOptions.gql
 * 
 * @param {number} productId - Product to load variants for
 * @param {string} cursor - GraphQL cursor of the last returned product variant * 
 * 
 * 
 * @returns {Promise}
 */
    async getOptions(cursor) {
    
        return this.gqlClient
        .query({
            query: customProductOptions,
            variables: { productId: parseInt(this.productId), pageSize: this.pageSize, cursor: cursor },
          }).then((data) => {
                let humanizedData = flattenGraphQLResponse(data)
                this.productVariants = [...this.productVariants, ...humanizedData.data.site.product.variants];
                if (data.data.site.product.variants.pageInfo.hasNextPage) {
                    return this.getOptions(data.data.site.product.variants.pageInfo.endCursor);
                } else {
                    return
                } 
          });
    }


/**
 * 
 * @param {Array.<Object>} variants 
 * 
 * Rendering a table with product variants
 */

    renderOptionsGrid(variants) {
        render(
            <ProductOptionsTable variants={variants}/>,
            document.getElementById("variantsForm"),
        );
        document.querySelector('#customFormOverlay').style.display = "none";
    }

/**
 * Creates a shopping cart
 * 
 * @param {Array.<Object>} lineitems 
 * 
 * @returns {Promise}
 */


    async createNewCart(lineItems) {
        const response = await fetch(`/api/storefront/carts`, {
            credentials: "include",
            method: "POST",
            body: JSON.stringify({ lineItems: lineItems })
        });
        await response.json();
        if (!response.ok) {
            return Promise.reject("There was an issue adding items to your cart. Please try again.")
        }
    }

/**
 * Adds products to an existing cart
 * 
 * @param {Array.<Object>} lineitems 
 * 
 * @returns {Promise}
 */

    async addToExistingCart(cart_id, lineItems) {
        const response = await fetch(`/api/storefront/carts/${cart_id}/items`, {
            credentials: "include",
            method: "POST",
            body: JSON.stringify({ lineItems: lineItems })
        });
        await response.json();
        if (!response.ok) {
            return Promise.reject("There was an issue adding items to your cart. Please try again.")
        }
    }

/**
 * Validation of input fields. Popup output with an error, if the check is not passed.
 * If the check is passed - call createCart function. 
 * 
 * @callback this.createCart
 */
    createLine() {
        let lineitems = [];
        let totalAmount = 0;
        let isError = false;
        let qtyFields = Array.prototype.slice.call(document.getElementsByClassName('qtyField'));
        for (const [i, item] of qtyFields.entries()) {
            let max = item.max == Infinity ? Infinity : parseInt(item.max);
            if (item.value > 0 && parseInt(item.value) <= max) {
                let lineItem = { "quantity": parseInt(item.value), "product_id": this.productId, "variant_id": this.productVariants[i].entityId }
                lineitems.push(lineItem);
                totalAmount += parseInt(item.value);
            } else if (item.value && parseInt(item.value) > max) {
                item.style.borderColor = "red";
                isError = true;
            }
        }
        if (isError) {
            return swal.fire({
                text: 'Please set the quantity of variants no greater than available.',
                icon: 'error',
            });        
        };
        if (lineitems.length < 1) {
            return swal.fire({
                text: 'Please set the quantity of at least 1 item.',
                icon: 'error',
            });  
        } else if (totalAmount > 1000000) {
            return swal.fire({
                text: 'Sorry, but the total number of products cannot exceed 1 million.',
                icon: 'error',
            }); 
        } else if (totalAmount < 1) {
            return swal.fire({
                text: 'Please set the quantity of at least 1 item.',
                icon: 'error',
            }); 
        } else if (this.productInStock && totalAmount > this.productInStock) {
            return swal.fire({
                text: `Sorry, but the total quantity of product variants cannot exceed ${this.productInStock}.`,
                icon: 'error',
            }); 
        } else {
            this.createCart(lineitems);
        }
    }

/**
 * Checking the cart. If the cart exists, we update it, if not - create new cart.
 * 
 * @param {Array.<Object>} lineitems 
 * 
 * @callback this.addToExistingCart
 * @callback this.createNewCart
 */

    createCart(lineitems) {
        document.querySelector('#customFormOverlay').style.display = "block";
        fetch(`/api/storefront/cart`)
        .then(response => response.json())
        .then(cart => {
            if (cart.length > 0) {
                return this.addToExistingCart(cart[0].id, lineitems)
            } else {
                return this.createNewCart(lineitems)
            }
        })
        .then(() => window.location = '/cart.php')
        .catch(err => alert(err));
    }

    onReady() {
        super.onReady();       
        
        Promise.all([this.getOptions()]).then(() => {

            this.renderOptionsGrid(this.productVariants);
            let qtyFields = document.querySelectorAll('.qtyField');
            $('#variantsFormSubmit').on('click', () => this.createLine());
            }
        )
    }
}